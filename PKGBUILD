# AArch64 Apple Silicon
# Maintainer: Philip Müller <philm@manjaro.org>
# Contributor: Hector Martin <marcan@marcan.st>

pkgname=m1n1
pkgver=1.2.9
pkgrel=1
pkgdesc='A bootloader for Apple Silicon'
_artwork_commit_id=19b5004529a9fa7d700f59881b7fd4e2f75271e3
_m1n1_commit_id=v${pkgver}
_artwork_srcname=asahi-artwork-${_artwork_commit_id}
_m1n1_srcname=m1n1-${_m1n1_commit_id##v}
arch=('aarch64')
url='http://asahilinux.org'
license=('MIT')
makedepends=( imagemagick ) # zopfli )
source=(
  "artwork-${_artwork_commit_id}.tar.gz::https://gitlab.manjaro.org/manjaro-arm/packages/apple-silicon/asahi-artwork/-/archive/${_artwork_commit_id}/asahi-artwork-${_artwork_commit_id}.tar.gz"
  #"https://gitlab.manjaro.org/artwork/design/-/raw/master/Manjaro_Logo_2022/logo_manjaro_rounded.svg"
  "bootlogo_128.png"
  "bootlogo_256.png"
  "m1n1-${_m1n1_commit_id}.tar.gz::https://github.com/AsahiLinux/m1n1/archive/${_m1n1_commit_id}.tar.gz"
)
sha256sums=('5c30276c36574d2a6759bd2e4d39ff99e0635aec6b454f6333a793f61019bfdd'
            '31a2c48c2c4b36c5877397ac724fe03107c97ea5e1b906c34d8facc37e9a97da'
            'cf0dea66fa24556222429abfc0211608e828df08ab72377053f8ab729e55ba1e'
            '135a24e407f7f9102f1eb0f359df82b88f0e9aaa367368daf140c6e63379c7b7')
b2sums=('34d2bb5c3bb872597cd237a65a706113143ce9efd1468dad853ae9b7a96e25c942d9723a8cc0c7acadf746312fbc69d886dfe050c60c316aa6e0a40a11813cb8'
        '8b1e67da88ff81938e09ae2a4af275582574f97b606a6a609bc1cc9d2bc94dae6c28d459b042071d7855f32b931ab5672110d5ae2d866f6a1eb53758e2cb096f'
        'e690e91fa7090d9a0f864d591df3b48b9532ba4d37c68b30f5b8ae5eecc56f080cdb2802382ea8f7727137b07baef3dd32d1027086fb2b90b338764ce500c696'
        'bb1514c5912f892e8cf5bf57656806abf2102cc4b370e06a9efbbeeeb90071d7358b921d8f24571ea3f6335fa9fc2b5734d9a6ca669ce783d75da314b88085e5')

prepare() {
  rm -rf "${srcdir}/$_m1n1_srcname"/artwork
  cp -r "${srcdir}/$_artwork_srcname" "${srcdir}/$_m1n1_srcname"/artwork
  
  # use our logo
  cd "${srcdir}/$_m1n1_srcname"/data
  rm bootlogo_{128,256}.{bin,png}
  cp "${srcdir}/bootlogo_128.png" .
  cp "${srcdir}/bootlogo_256.png" .
  #convert -background none -resize 128x128 -gravity center -extent 128x128 \
  #  ${srcdir}/logo_manjaro_rounded.svg bootlogo_128.png
  #zopflipng -ym bootlogo_128.png bootlogo_128.png
  #convert -background none -resize 256x256 -gravity center -extent 256x256 \
  #  ${srcdir}/logo_manjaro_rounded.svg bootlogo_256.png
  #zopflipng -ym bootlogo_256.png bootlogo_256.png
  ./makelogo.sh
}

build() {
  cd "${srcdir}/$_m1n1_srcname"
  make ARCH= RELEASE=1
}

package() {
  cd "${srcdir}"

  tgtdir="$pkgdir/usr/lib/asahi-boot"

  install -Dt "$tgtdir" -m644 "${_m1n1_srcname}/build/m1n1.bin"

  install -Dm644 "$srcdir/$_m1n1_srcname/LICENSE" \
    "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}
